package org.lizf.service;



import org.lizf.dto.Exposer;
import org.lizf.dto.SeckillExecution;
import org.lizf.entity.Seckill;
import org.lizf.exception.RepeatKillException;
import org.lizf.exception.SeckillClosedException;
import org.lizf.exception.SeckillException;

import java.util.List;

/**
 * 站在使用者的角度设计接口：
 * 1、方法定义粒度
 * 2、参数
 * 3、返回类型：return/exception
 */
public interface SeckillService {
    /**
     * 秒杀列表
     * @return
     */
    List<Seckill> getSecKillList();

    /**
     * 查询单个秒杀
     * @return
     */
    Seckill getSecKillById(long seckillId);

    /**
     * 秒杀开启时输出秒杀接口地址
     * 否则输出系统时间和秒杀时间
     * @param seckillId
     */
    Exposer exportSeckillURL(long seckillId, long userPhone);

    /**
     * 执行秒杀操作
     * @param seckillId
     * @param userPhone
     * @param md5
     */
    SeckillExecution executeSeckill(long seckillId, long userPhone, String md5) throws SeckillException, RepeatKillException, SeckillClosedException;


}
