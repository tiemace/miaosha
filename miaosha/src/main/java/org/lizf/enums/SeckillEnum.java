package org.lizf.enums;

public enum SeckillEnum {
    SUCCESS(1, "秒杀成功"),
    END(0, "秒杀结束"),
    REPEAT_KILL(-1, "重复秒杀"),
    DATE_REWRITE(-2, "数据篡改"),
    INNER_ERROR(-100, "系统错误")

    ;

    private int state;
    private String stateInfo;

    SeckillEnum(int state, String stateInfo) {
        this.state = state;
        this.stateInfo = stateInfo;
    }

    public static SeckillEnum getMsg(int key) {
        for (SeckillEnum se : values()) {
            if(se.getState() == key) {
                return se;
            }
        }
        return null;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getStateInfo() {
        return stateInfo;
    }

    public void setStateInfo(String stateInfo) {
        this.stateInfo = stateInfo;
    }
}
