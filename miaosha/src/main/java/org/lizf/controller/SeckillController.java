package org.lizf.controller;

import org.lizf.service.SeckillService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SeckillController {

    @Autowired
    private SeckillService seckillService;

    @GetMapping("/hello")
    @ResponseBody
    public String hello() {
        return seckillService.getSecKillList().toString();
    }
}
