package org.lizf.dao;

import org.apache.ibatis.annotations.Param;
import org.lizf.entity.SuccessKilled;

public interface SuccessKilledDAO {
    /**
     * 插入购买明细，可过滤重复，联合主键
     * @param seckillId
     * @param userPhone
     * @return 插入的行数
     */
    int insertSuccessKilled(@Param("seckillId") long seckillId, @Param("userPhone") long userPhone);

    /**
     * 根据id查询SuccessKilled并携带秒杀产品对象实体
     * @param seckillId
     * @return
     */
    SuccessKilled queryByIdAndPhoneWithSeckill(@Param("seckillId") long seckillId, @Param("userPhone") long userPhone);
}
