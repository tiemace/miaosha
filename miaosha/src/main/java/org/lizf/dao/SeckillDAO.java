package org.lizf.dao;


import org.apache.ibatis.annotations.Param;
import org.lizf.entity.Seckill;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

public interface SeckillDAO {
    /**
     * 减库存
     * @param seckillId
     * @param killTime
     * @return 如果影响行数>1，表示更新记录行数
     */
    int reduceNumber(@Param("seckillId") long seckillId, @Param("killTime") Date killTime);

    /**
     * 根据id查找秒杀对象
     * @param seckillId
     * @return
     */
    Seckill queryById(long seckillId);

    /**
     * 查询所有秒杀对象
     * @param offset
     * @param limit
     * @return
     */
    List<Seckill> queryAll(@Param("offset") int offset, @Param("limit") int limit);}
