package org.lizf.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest
public class SeckillServiceTest {
    Logger logger = LoggerFactory.getLogger(this.getClass());
    @Autowired
    private SeckillService seckillService;

    @Test
    public void getSecKillList() throws Exception {
        logger.info(seckillService.getSecKillList().toString());
    }

    @Test
    public void getSecKillById() throws Exception {
        logger.info(seckillService.getSecKillById(1000).toString());
    }

    @Test
    public void exportSeckillURL() throws Exception {

        logger.info("url = {}", seckillService.exportSeckillURL(1000, 18629394128L));
    }

    @Test
    public void executeSeckill() throws Exception {
    }

}